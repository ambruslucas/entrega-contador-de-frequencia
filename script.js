

document.getElementById("countButton").onclick = function() {  

    let divWordPrint;

    let letterCounts = {};

    let words = {};

    let wordsCount = {};

    let wordsDiv = document.getElementById("wordsDiv");
    let lettersDiv = document.getElementById("lettersDiv"); 

    lettersDiv.innerHTML = '' 
    wordsDiv.innerHTML = ''

    let typedText = document.getElementById("textInput").value;

    words = typedText.split(/\s/);

    typedText = typedText.toLowerCase(); 
    typedText = typedText.replace(/[^a-z'\s]+/g, ""); 

    for (let i = 0; i < typedText.length; i++) {
        currentLetter = typedText[i];

        if (letterCounts[currentLetter] === undefined) {
            letterCounts[currentLetter] = 1; 
        } else { 
            letterCounts[currentLetter]++; 
        }
        
    }

    for (let index = 0; index < words.length; index++) {
        currentWord = words[index];

        if (wordsCount[currentWord] === undefined) {
            wordsCount[currentWord] = 1; 
        } else { 
            wordsCount[currentWord]++; 
        }

    }

    for (let letter in letterCounts) {
        let divLetterPrint = document.createElement("divLetterPrint");
        let textContent = document.createTextNode('"' + letter + "\": " + letterCounts[letter] + " ");
        divLetterPrint.appendChild(textContent);
        lettersDiv.appendChild(divLetterPrint);
    
    }

    for (let word in wordsCount) { 
        divWordPrint = document.createElement("divWordPrint"); 
        let textWords = document.createTextNode('"' + word + "\": " + wordsCount[word] + " ");
        divWordPrint.appendChild(textWords);
        wordsDiv.appendChild(divWordPrint);

        
      }
    
     

      console.log(letterCounts);
      console.log(wordsCount)
      
    }
    
